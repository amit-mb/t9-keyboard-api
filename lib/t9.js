const fs = require("fs");
const keys = {
  a: 2,
  b: 2,
  c: 2,
  d: 3,
  e: 3,
  f: 3,
  g: 4,
  h: 4,
  i: 4,
  j: 5,
  k: 5,
  l: 5,
  m: 6,
  n: 6,
  o: 6,
  p: 7,
  q: 7,
  r: 7,
  s: 7,
  t: 8,
  u: 8,
  v: 8,
  w: 9,
  x: 9,
  y: 9,
  z: 9
};

class Trie {
  constructor() {
    this.children = {};
    this.words = [];
  }
  insert(word) {
    let nodeToAddWord = this.traverseAddingNodes(this, word);
    this.insertWordIntoList(nodeToAddWord.words, word);
  }
  traverseAddingNodes(node, word) {
    let i = 0,
      len = word.length;
    for (i, len; i < len; i++) {
      let thisLetter = word[i];
      let thisKey = keys[thisLetter];

      if (node.children.hasOwnProperty(thisKey)) {
        node = node.children[thisKey];
      } else {
        break;
      }
    }
    for (i, len; i < len; i++) {
      let thisLetter = word[i];
      let thisKey = keys[thisLetter];
      node.children[thisKey] = new Trie();
      node = node.children[thisKey];
    }

    return node;
  }
  insertWordIntoList(list, word) {
    let wordToInsert = [word];
    let wordsLength = list.length;
    if (wordsLength === 0) {
      list.push(wordToInsert);
    } else {
      list.splice(wordsLength, 0, wordToInsert);
    }
  }

  getSuggestions(keyString, suggestionDepth, deepSuggestions = []) {
    console.log(keyString);
    let result = [];
    let node = this;
    let flag = 0;
    for (let i = 0; i < keyString.length; i++) {
      if (!node.children[keyString[i]]) {
        flag = 1;
        break;
      }
      let thisKey = keyString[i];
      node = node.children[thisKey];
    }
    if (flag == 1) {
      return [];
    }
    if (!node) return;
    result = result.concat(
      node.words.map(function(wordTuple) {
        return wordTuple[0];
      })
    );

    return suggestionDepth > 0
      ? result.concat(this.getDeeperSuggestions(node, suggestionDepth, deepSuggestions))
      : result;
  }
  getDeeperSuggestions(root, maxDepth, deepSuggestions) {
    while (deepSuggestions.length < maxDepth) {
      deepSuggestions.push([]);
    }
    this.traverse(root, 0, maxDepth, deepSuggestions);
    deepSuggestions = deepSuggestions.map(function(level) {
      return level.sort(function(a, b) {
        return b[1] - a[1];
      });
    });

    return deepSuggestions.reduce(function(result, level) {
      return result.concat(
        level.map(function(wordTuple) {
          return wordTuple[0];
        })
      );
    }, []);
  }
  traverse(root, depth, maxDepth, deepSuggestions) {
    if (depth <= maxDepth && depth !== 0) {
      let d = depth - 1;
      deepSuggestions[d] = deepSuggestions[d].concat(root.words);
    }

    if (depth === maxDepth) {
      return;
    }

    for (let childKey in root.children) {
      this.traverse(root.children[childKey], depth + 1, maxDepth, deepSuggestions);
    }
  }
}

let trie = new Trie();

let initialize = () => {
  fs.readFileSync("./dictionary.txt")
    .toString()
    .split("\n")
    .forEach(words => {
      trie.insert(words.toLowerCase());
    });
};
getSuggestions = query => {
  return trie.getSuggestions(query, 0);
};

getRecommendation = query => {
  return trie.getSuggestions(query, 30);
};

module.exports = {
  initialize: initialize,
  getSuggestions: getSuggestions,
  getRecommendation: getRecommendation
};
