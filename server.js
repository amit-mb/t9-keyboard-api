const express = require("express");
const app = express();
const t9 = require("./lib/t9.js");
t9.initialize();

let port = process.env.PORT || 3000;

app.get("/getT9Suggestions", (req, res) => {
  let suggestedWords = {
    data: t9.getSuggestions(req.query.numbers),
    recommendations: t9.getRecommendation(req.query.numbers)
  };
  res.send(suggestedWords);
});

app.listen(port || 3000, () => {
  console.log(`Server listening at Port ${port}`);
});
